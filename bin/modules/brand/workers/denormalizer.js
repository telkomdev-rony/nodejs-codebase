'use strict';

const model = require('../repositories/queries/query_model');
const validate = require("validate.js")
const command = require('../repositories/commands/command');

const createOneBrandView = async (payload) => {
    const view = model.generalBrand();
    view.id = payload.id;
    view.code = payload.code;
    view.name = payload.name;
    if(!validate.isEmpty(payload.logo)){view.logo = payload.logo;}
    if(!validate.isEmpty(payload.createdAt)){view.createdAt = payload.createdAt;}
    if(!validate.isEmpty(payload.createdBy)){view.createdBy = payload.createdBy;}
    if(!validate.isEmpty(payload.updatedAt)){view.updatedAt = payload.updatedAt;}
    if(!validate.isEmpty(payload.updatedBy)){view.updatedBy = payload.updatedBy;}
    const document = view;  
    const result = await command.insertOneGeneralBrandView(document);
    return result;
}

const updateOneBrandView = async (payload) => {
    const view = model.generalBrand()
    view.code = payload.code
    view.name = payload.name
    if(!validate.isEmpty(payload.logo)){view.logo = payload.logo}
    if(!validate.isEmpty(payload.createdAt)){view.createdAt = payload.createdAt}
    if(!validate.isEmpty(payload.createdBy)){view.createdBy = payload.createdBy}
    if(!validate.isEmpty(payload.updatedAt)){view.updatedAt = payload.updatedAt}
    if(!validate.isEmpty(payload.updatedBy)){view.updatedBy = payload.updatedBy}  
    const document = view
    const result = await command.updateOneGeneralBrandView(document);
    return result
}

module.exports = {
    createOneBrandView: createOneBrandView,
    updateOneBrandView: updateOneBrandView
}