'use strict';

const query = require('./query');
const model = require('./query_model');
const wrapper = require('../../../../helpers/utils/wrapper');

class Brand{
    constructor(id=``){
        this.id = id;
    }

    async viewOneBrand(id=this.id){
        const result = await query.findOneBrandById(id);
        if(result.err){
            return result;
        }else{
            const data = [result.data];
            let view = model.generalBrand();
            view = data.reduce((accumulator, value) => {
                accumulator.id = value.id;
                accumulator.code = value.code;
                accumulator.name = value.name;
                accumulator.logo = value.logo;
                accumulator.createdAt = value.createdAt;
                accumulator.createdBy = value.createdBy;
                accumulator.updatedAt = value.updatedAt;
                accumulator.updatedBy = value.updatedBy;
                return accumulator;
            }, view)
            return wrapper.data(view);
        }
    }

    async viewManyBrand(queryParser){
        let data = [];
        const result = await query.findManyBrandById(queryParser);
        if(result.err){
            return result;
        }else{
            data = result.data.map(value => {
                let brandData = model.generalBrand()
                brandData.id = value.id;
                brandData.code = value.code;
                brandData.name = value.name;
                brandData.logo = value.logo;
                brandData.createdAt = value.createdAt;
                brandData.createdBy = value.createdBy;
                brandData.updatedAt = value.updatedAt;
                brandData.updatedBy = value.updatedBy;
                return brandData;
            })
            return wrapper.data(data);
        }
    }

    async viewOneBrandName(name){
        let data = []
        const result = await query.findOneBrandByName(name)
        if(result.err){
            return result
        }else{
            data = result.data.map(value => {
                let view = model.generalBrand()
                view.id = value.id
                view.code = value.code
                view.name = value.name
                view.logo = value.logo
                view.createdAt = value.createdAt
                view.createdBy = value.createdBy
                view.updatedAt = value.updatedAt
                view.updatedBy = value.updatedBy
                return view
            })
            return wrapper.data(data)
        }
    }
}

module.exports = Brand;