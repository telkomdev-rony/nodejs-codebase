'use strict';

const MSSQL = require('../../../../helpers/databases/mssql/db');
const Mongo = require('../../../../helpers/databases/mongodb/db');
const wrapper = require('../../../../helpers/utils/wrapper');
const config = require('../../../../infra/configs/global_config');

const findOneBrandById = async (id) => {
    const db = new Mongo(config.getMongoProductView());
    db.setCollection('brand');
    const parameter = {id:id};
    const result = await db.findOne(parameter);
    return result;
}

const findManyBrandById = async (queyParser) => {
    const db = new Mongo(config.getMongoProductView());
    db.setCollection('brand');
    const result = await db.findMany(queyParser);
    return result;
}

const findOneBrandByName = async (name) => {
    const db = new Mongo(config.getMongoProductView());
    db.setCollection('brand');
    const parameter = {name:name}
    const result = await db.findMany(parameter)
    return result
}

module.exports = {
    findOneBrandById: findOneBrandById,
    findManyBrandById: findManyBrandById,
    findOneBrandByName: findOneBrandByName
}