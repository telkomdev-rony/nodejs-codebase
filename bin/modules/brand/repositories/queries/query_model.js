'use strict';

const generalBrand = () => {
    const model = {
        id:``,
        code:``,
        name:``,
        logo:``,
        createdAt:``,
        createdBy:``,
        updatedAt:``,
        updatedBy:``    
    }
    return model;
}

module.exports = {
    generalBrand: generalBrand
}