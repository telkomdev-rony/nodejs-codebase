'use strict';

const command = require('./command');
const model = require('./command_model');
const Emitter = require('../../../../helpers/events/event_emitter');
const validate = require("validate.js");

class Brand{
    constructor(id=``){
        this.id = id;
    }

    async addNewBrandFromTlu_InveCPBrand(message){
        const result = await command.insertOneBrandFromTlu_InveCPBrand(message);
        return result;
    }

    async addNewBrandFromTlu_InveCPBrandToEventStore (message){
        const data = [message.payload];
        let view = model.generalBrand();
        view = data.reduce((accumulator, value) => {
            if(!validate.isEmpty(value.vBrandID)){accumulator.id = value.vBrandID;}
            if(!validate.isEmpty(value.vCode)){accumulator.code = value.vCode;}
            if(!validate.isEmpty(value.vName)){accumulator.name = value.vName;}
            if(!validate.isEmpty(value.vLogo)){accumulator.logo = value.vLogo;}
            if(!validate.isEmpty(value.vCreatorDateTime)){accumulator.createdAt = value.vCreatorDateTime;}
            if(!validate.isEmpty(value.vCreatorNo)){accumulator.createdBy = value.vCreatorNo;}
            if(!validate.isEmpty(value.vEditorDateTime)){accumulator.updatedAt = value.vEditorDateTime;}
            if(!validate.isEmpty(value.vEditorNo)){accumulator.updatedBy = value.vEditorNo;}
            return accumulator;
        }, view)
        message.payload = view;
        const document = message;
        const result = await command.insertOneBrandFromTlu_InveCPBrandToEventStore(document);
        return result;
    }

    async editNewBrandFromTlu_InveCPBrand(message){
        const result = await command.updateOneBrandFromTlu_InveCPBrand(message)
        return result
    }

    async editNewBrandFromTlu_InveCPBrandToEventStore (message){
        const data = [message.payload]
        let view = model.generalBrand()
        view = data.reduce((accumulator, value) => {
            if(!validate.isEmpty(value.vBrandID)){accumulator.id = value.vBrandID;}
            if(!validate.isEmpty(value.vCode)){accumulator.code = value.vCode;}
            if(!validate.isEmpty(value.vName)){accumulator.name = value.vName;}
            if(!validate.isEmpty(value.vLogo)){accumulator.logo = value.vLogo;}
            if(!validate.isEmpty(value.vCreatorDateTime)){accumulator.createdAt = value.vCreatorDateTime;}
            if(!validate.isEmpty(value.vCreatorNo)){accumulator.createdBy = value.vCreatorNo;}
            if(!validate.isEmpty(value.vEditorDateTime)){accumulator.updatedAt = value.vEditorDateTime;}
            if(!validate.isEmpty(value.vEditorNo)){accumulator.updatedBy = value.vEditorNo;}
            return accumulator
        }, view)
        message.payload = view
        const document = message
        const result = await command.updateOneBrandFromTlu_InveCPBrandToEventStore(document)
        return result
    }

    publishEventBrandCreatedFromTlu_InveCPBrand(document){
        Emitter.emitEvent('tlu_InveCPBrandEventingCreated',document);
    }

    publishEventBrandCreatedFromTlu_InveCPBrandToEventStore(payload){
        
        Emitter.emitEvent('tlu_InveCPBrandToEventStoreCreated',payload);
    }

    publishEventBrandUpdatedFromTlu_InveCPBrand(document){
        Emitter.emitEvent('tlu_InveCPBrandEventingUpdated',document)
    }

    publishEventBrandUpdatedFromTlu_InveCPBrandToEventStore(payload){
        Emitter.emitEvent('tlu_InveCPBrandToEventStoreUpdated',payload)
    }
}

module.exports = Brand;