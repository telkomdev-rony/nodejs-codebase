'use strict';

const MSSQL = require('../../../../helpers/databases/mssql/db');
const Mongo = require('../../../../helpers/databases/mongodb/db');
const wrapper = require('../../../../helpers/utils/wrapper');
const config = require('../../../../infra/configs/global_config');

const insertOneBrandFromTlu_InveCPBrand = async (document) => {
    const db = new Mongo(config.getMongoCoreDOS());
    db.setCollection('tlu_InveCPBrand');
    const result = await db.insertOne(document);
    return result;
}

const insertOneBrandFromTlu_InveCPBrandToEventStore = async (document) => {
    const db = new Mongo(config.getMongoProductES());
    db.setCollection('brand');
    const result = await db.insertOne(document);
    return result;
}

const insertOneGeneralBrandView = async (document) => {
    delete document._id;
    const db = new Mongo(config.getMongoProductView());
    db.setCollection('brand');
    const result = await db.insertOne(document);
    return result;
}

const updateOneBrandFromTlu_InveCPBrand = async (document) => {
    const db = new Mongo (config.getMongoCoreDOS())
    db.setCollection('tlu_InveCPBrand')
    const parameter = {"payload.vBrandID":document.payload.vBrandID}
    const result = await db.upsertOne(parameter,document)
    return result
}

const updateOneBrandFromTlu_InveCPBrandToEventStore = async (document) => {
    const db = new Mongo (config.getMongoProductES())
    db.setCollection('brand')
    const parameter = {"payload.id":document.payload.id}
    const result = await db.upsertOne(parameter,document)
    return result
}

const updateOneGeneralBrandView = async (document) => {
    delete document._id;
    const db = new Mongo(config.getMongoProductView());
    db.setCollection('brand');
    const parameter = {"id": document.id}
    const result = await db.upsertOne(parameter,document);
    return result;
}

module.exports = {
    insertOneBrandFromTlu_InveCPBrand: insertOneBrandFromTlu_InveCPBrand,
    insertOneBrandFromTlu_InveCPBrandToEventStore: insertOneBrandFromTlu_InveCPBrandToEventStore,
    insertOneGeneralBrandView: insertOneGeneralBrandView,
    updateOneBrandFromTlu_InveCPBrand: updateOneBrandFromTlu_InveCPBrand,
    updateOneBrandFromTlu_InveCPBrandToEventStore: updateOneBrandFromTlu_InveCPBrandToEventStore,
    updateOneGeneralBrandView: updateOneGeneralBrandView
}