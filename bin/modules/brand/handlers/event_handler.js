'use strict';

const Emitter = require('../../../helpers/events/event_emitter');
const commandHandler = require('../repositories/commands/command_handler');
const denormalizer = require('../workers/denormalizer');

const tlu_InveCPBrandCreatedHandler = async (message, receipt, sqs) => {
    const result = await commandHandler.tlu_InveCPBrandCreatedCommand(message);
    //enable this if you have accomplished the pattern
    (result.err) ? Emitter.emitEvent(`error`, result.err) : Emitter.emitEvent('eventHandled',receipt,sqs);
}

const tlu_InveCPBrandEventingCreatedHandler = async (message) => {
    const result = await commandHandler.tlu_InveCPBrandEventingCreatedCommand(message);
    (result.err) ? Emitter.emitEvent(`error`, result.err) : console.log(`tlu_InveCPBrand has been Proceed to Event Store`);
}

const tlu_InveCPBrandToEventStoreCreatedHandler = async (data) => {
    const result = await denormalizer.createOneBrandView(data.payload);
    (result.err) ? Emitter.emitEvent(`error`, result.err) : console.log(`View of General Brand has been Created`);
}

const tlu_InveCPBrandUpdatedHandler = async (message, receipt, sqs) => {
    const result = await commandHandler.tlu_InveCPBrandUpdatedCommand(message);
    (result.err) ? Emitter.emitEvent(`error`, result.err) : Emitter.emitEvent('eventHandled',receipt,sqs);
}

const tlu_InveCPBrandUpdatedtoEventStoreHandler = async (message) => {
    const result = await commandHandler.tlu_InveCPBrandEventingUpdatedCommand(message);
    (result.err) ? Emitter.emitEvent(`error`, result.err) : console.log(`tlu_InveCPBrand has been Proceed to Event Store`);
}

const tlu_InveCPBrandUpdatedToViewHandler = async (data) => {
    const result = await denormalizer.updateOneBrandView(data.payload);
    (result.err) ? Emitter.emitEvent(`error`, result.err) : console.log(`View of General Brand has been Updated`);
}

module.exports= {
    tlu_InveCPBrandCreatedHandler: tlu_InveCPBrandCreatedHandler,
    tlu_InveCPBrandEventingCreatedHandler: tlu_InveCPBrandEventingCreatedHandler,
    tlu_InveCPBrandToEventStoreCreatedHandler: tlu_InveCPBrandToEventStoreCreatedHandler,
    tlu_InveCPBrandUpdatedHandler: tlu_InveCPBrandUpdatedHandler,
    tlu_InveCPBrandUpdatedtoEventStoreHandler: tlu_InveCPBrandUpdatedtoEventStoreHandler,
    tlu_InveCPBrandUpdatedToViewHandler: tlu_InveCPBrandUpdatedToViewHandler
}