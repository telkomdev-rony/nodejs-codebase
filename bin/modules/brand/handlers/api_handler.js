'use strict';

const wrapper = require('../../../helpers/utils/wrapper');
const validator = require('../utils/brand_validator');
const queryParser = require('../utils/brand_query_parser');
const queryHandler = require('../repositories/queries/query_handler');

const getOneBrand = async (req, res, next) => {
  const validateParam = await validator.isValidParamGetOneBrand(req);
  const getData = async (result) => {
    if(result.err){
      return result;
    }else{
      const id = req.params.id.toUpperCase();
      return await queryHandler.queryOneBrand(id);
    }
  }
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res,'error',result) :
    wrapper.response(res,'success',result,`Brand Found by ${req.params.id}`);
  }
  sendResponse(await getData(validateParam));
}

const getManyBrand = async (req, res, next) => {
  const validateParam = await validator.isValidParamGetManyBrand(req);
  const getData = async (result) => {
    if (result.err) {
      return result;
    } else {
      const parseQueryBrand = await queryParser.queryParserGetManyBrand(req);
      return queryHandler.queryManyBrand(parseQueryBrand);
    }
  }
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res,'error',result) :
    wrapper.response(res,'success',result,`Brand Found by ${req.params.brandId}`);
  }
  sendResponse(await getData(validateParam));
}

const getOneBrandByName = async (req, res, next) => {
  const getData = async (result) => {
    if(result.err){
      return result
    }else{
      const name = req.params.name.toUpperCase();
      return await queryHandler.queryBrandName(name)
    }
  }
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res, 'error', result) : 
    wrapper.response(res, 'success', result, `Brand Found by ${req.params.name}`);
  }
  sendResponse(await getData(req.params.name))
}

module.exports = {
  getOneBrand: getOneBrand,
  getManyBrand: getManyBrand,
  getOneBrandByName: getOneBrandByName
}