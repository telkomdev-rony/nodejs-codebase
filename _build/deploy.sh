aws ecs register-task-definition --family squid-task --cli-input-json file://$WORKSPACE/_build/msi-api-service-product.json > $WORKSPACE/_build/msi-api-service-product_output.json
export PRODUCT_TASK_ARN=$(jq '.taskDefinition.taskDefinitionArn' msi-api-service-product_output.json | sed -e 's/^"//' -e 's/"$//')
export PRODUCT_TASK_FAMILY=$(jq '.taskDefinition.family' msi-api-service-product_output.json | sed -e 's/^"//' -e 's/"$//')
export PRODUCT_TASK_REVISION=$(jq '.taskDefinition.revision' msi-api-service-product_output.json)
aws ecs update-service --cluster squid-cluster --service msi-api-service-product --task-definition $PRODUCT_TASK_FAMILY:$PRODUCT_TASK_REVISION
